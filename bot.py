import json

from discord.ext import commands
from discord.ext.tasks import loop
import discord

from twitch import get_notifications
from twitch import get_user_information

bot = commands.Bot(command_prefix="$")
#channelStream = 886554898760630312 #live
channelLog = 886554898760630312
channelStream = 886554898760630312 #debug


@bot.command()
async def ping(ctx):
    await ctx.send("pong")


@bot.event
async def on_ready():
    print('Logged on as {0}!'.format(bot.user.name))
    channel = bot.get_channel(channelLog)
    await channel.send('Logged on as {0}!'.format(bot.user.name))


@loop(seconds=90)
async def check_twitch_online_streamers():
    channel = bot.get_channel(channelStream)
    if not channel:
        return

    notifications = get_notifications()
    for notification in notifications:
        print(notification)
        if "empirerp.de" not in notification["title"].lower():
            return

        user_information = get_user_information(notification['user_id'])

        embed = discord.Embed(
            title=notification["title"],
            description=notification['game_name'] + ' wird gestreamt',
            colour=discord.Colour.dark_purple(),
            url='https://twitch.tv/' + notification["user_login"]
        )
        embed.set_author(name=notification["user_name"], icon_url=user_information["profile_image_url"])
        embed.set_image(url=(notification["thumbnail_url"].replace('{width}', '1920').replace('{height}', '1080')))
        # await channel.send("streamer {} ist jetzt online: {}".format(notification["user_login"], notification))
        await channel.send(embed=embed)
        await bot.get_channel(channelLog).send(notification["user_name"] + " is online with: " + notification["title"])
        print(notification["user_name"] + " is online with: " + notification["title"])


with open("config.json") as config_file:
    config = json.load(config_file)

if __name__ == "__main__":
    check_twitch_online_streamers.start()
    bot.run(config["discord_token"])
